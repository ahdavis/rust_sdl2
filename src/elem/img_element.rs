/*
 * img_element.rs
 * Defines a class that represents an image
 * Created by Andrew Davis
 * Created on 1/25/2019
 * Copyright 2019 Andrew Davis
 * Licensed under the Lesser GNU GPL, version 3
 */

//usage statements
use super::Element;
use super::super::Surface;

/// A image capable of being drawn to the screen
pub struct ImgElement {
    /// The `Surface` holding the image
    _surface: Surface,
    /// The path to the image file
    _path: String,
}

//method implementations
impl ImgElement {
    /// Returns an `ImgElement` object created from a BMP image,
    /// wrapped in a `Result`
    /// 
    /// # Arguments
    ///
    /// * `path` - The path to the BMP image 
    pub fn from_bmp(path: &str) -> Result<Self, String> {
        //get the Surface instance from the image path
        let surface_res = Surface::from_bmp(path);

        //check it for errors and return the result
        match surface_res {
            Ok(surface) => {
                return Ok(ImgElement {
                    _surface: surface,
                    _path: path.to_owned(),
                });
            }

            Err(msg) => {
                return Err(msg);
            }
        }
    }

    /// Returns the path to the stored image
    pub fn get_path(&self) -> &str {
        return self._path.as_str();
    }
}

// Trait implementations

//Element trait
impl Element for ImgElement {
    /// Returns the underlying `Surface` of the `ImgElement`
    fn get_surface(&self) -> &Surface {
        return &self._surface;
    }
}

//end of file
