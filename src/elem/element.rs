/*
 * element.rs
 * Defines a trait that is implemented by graphics elements
 * Created by Andrew Davis
 * Created on 1/25/2019
 * Copyright 2019 Andrew Davis
 * Licensed under the Lesser GNU GPL, version 3
 */

//usage statement
use super::super::Surface;

/// Defines a graphics element capable of being drawn to the screen
pub trait Element {
    /// Returns the `Surface` object that contains the graphics data
    fn get_surface(&self) -> &Surface;
}

//end of file
