//! Rust SDL2
//!
//! `rust_sdl2` contains Rust bindings for the SDL2 graphics library

//module imports
mod c_iface;
mod ctxt;
mod win;
mod util;
mod elem;
mod event;
mod input;

//usage statements
pub use ctxt::Context;
pub use win::Window;
pub use win::Surface;
pub use util::Color;
pub use util::Rect;
pub use util::Point;
pub use util::Circle;
pub use elem::Element;
pub use elem::ImgElement;
pub use event::EventType;
pub use event::Event;
pub use input::Keycode;

//end of file
