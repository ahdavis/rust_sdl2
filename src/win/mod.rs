/*
 * mod.rs
 * Module header for rust_sdl2 window files
 * Created by Andrew Davis
 * Created on 1/20/2019
 * Copyright 2019 Andrew Davis
 * Licensed under the Lesser GNU GPL, version 3
 */

//module import statements
mod window;
mod surface;

//usage statements
pub use window::Window;
pub use surface::Surface;

//end of header
