/*
 * window.rs
 * Defines a class that represents a graphics window
 * Created by Andrew Davis
 * Created on 1/20/2019
 * Copyright 2019 Andrew Davis
 * Licensed under the Lesser GNU GPL, version 3
 */

//usage statements
use super::super::c_iface::sdl;
use std::ffi::CString;
use std::ffi::CStr;
use std::os::raw::c_int;
use std::ptr;

/// A graphics window that can be drawn on
pub struct Window {
    /// The internal window data 
    _data: *mut sdl::SDL_Window,
    /// The caption of the `Window`
    _caption: String,
    /// The width of the `Window` (in pixels)
    _width: u32,
    /// The height of the `Window` (in pixels)
    _height: u32,
    /// Is the `Window` currently being shown?
    _shown: bool,
}

//method implementations
impl Window {
    /// Returns a new `Window` object wrapped in a `Result`
    ///
    /// # Arguments
    ///
    /// * `caption` - The caption of the `Window`
    /// * `width` - The width of the `Window`
    /// * `height` - The height of the `Window`
    /// * `should_show` - Whether the `Window` should be shown immediately
    pub fn new(caption: &str, width: u32, height: u32,
                    should_show: bool) -> Result<Self, String> {
        unsafe {
            //convert the caption string to a CStr
            let ccaption = CString::new(caption).unwrap();

            //get whether the window should be shown
            let mut show_flag = sdl::SDL_WindowFlags_SDL_WINDOW_HIDDEN;
            if should_show {
                show_flag = sdl::SDL_WindowFlags_SDL_WINDOW_SHOWN;
            } 

            //attempt to create the data pointer
            let data = sdl::SDL_CreateWindow(
                            ccaption.as_ptr(),
                            sdl::SDL_WINDOWPOS_UNDEFINED_MASK as c_int,
                            sdl::SDL_WINDOWPOS_UNDEFINED_MASK as c_int,
                            width as c_int,
                            height as c_int, show_flag);

            //make sure that it was created successfully
            if data.is_null() { //if creation failed
                //then get the error message
                let err = CStr::from_ptr(sdl::SDL_GetError());
    
                //and return an error
                return Err(
                    format!("Window {} could not be created! Message: {}",
                                caption, err.to_str().unwrap()));
            }

            //construct and return the window
            return Ok(Window {
                _data: data,
                _caption: caption.to_owned(),
                _width: width,
                _height: height,
                _shown: should_show,
            });
        }
    }

    /// Returns the `Window`'s data pointer
    ///
    /// FOR INTERNAL USE ONLY - DO NOT USE
    pub fn get_data(&mut self) -> *mut sdl::SDL_Window {
        return self._data;
    }

    /// Returns the caption of the `Window`
    pub fn get_caption(&self) -> &str {
        return self._caption.as_str();
    }

    /// Returns the width of the `Window`
    pub fn get_width(&self) -> u32 {
        return self._width;
    }

    /// Returns the height of the `Window`
    pub fn get_height(&self) -> u32 {
        return self._height;
    }

    /// Returns the internal ID of the `Window`
    pub fn get_id(&self) -> u32 {
        unsafe {
            return sdl::SDL_GetWindowID(self._data);
        }
    }

    /// Returns whether the `Window` is currently being shown
    pub fn is_shown(&self) -> bool {
        return self._shown;
    }

    /// Sets the `Window`'s caption
    /// 
    /// # Arguments
    ///
    /// * `caption` - The new caption of the `Window`
    pub fn set_caption(&mut self, caption: &str) {
        self._caption = caption.to_owned();
        unsafe {
            let ccaption = CString::new(caption).unwrap();
            sdl::SDL_SetWindowTitle(self._data, ccaption.as_ptr());
        }
    }

    /// Shows the `Window`
    pub fn show(&mut self) {
        unsafe {
            sdl::SDL_ShowWindow(self._data);
        }
        self._shown = true;
    }

    /// Hides the `Window`
    pub fn hide(&mut self) {
        unsafe {
            sdl::SDL_HideWindow(self._data);
        }
        self._shown = false;
    }

    /// Updates the `Window`
    pub fn update(&mut self) {
        unsafe {
            sdl::SDL_UpdateWindowSurface(self._data);
        }
    }
}

//trait implementations

// Drop trait
impl Drop for Window {
    /// Deallocates a `Window` instance
    fn drop(&mut self) {
        unsafe {
            sdl::SDL_DestroyWindow(self._data);
            self._data = ptr::null_mut();
        }
    }
}

//end of class
