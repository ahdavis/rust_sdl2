/*
 * surface.rs
 * Defines a class that represents a drawable surface
 * Created by Andrew Davis
 * Created on 1/20/2019
 * Copyright 2019 Andrew Davis
 * Licensed under the Lesser GNU GPL, version 3
 */

//usage statements
use super::super::c_iface::sdl;
use super::super::Color;
use super::super::Element;
use super::super::Rect;
use super::window::Window;
use std::ptr;
use std::ffi::CStr;
use std::ffi::CString;

/// Defines a surface capable of being drawn to
pub struct Surface {
    /// The internal surface data 
    _data: *mut sdl::SDL_Surface,
    /// is the `Surface` derived from a `Window`?
    _is_window_surface: bool,
}

//method implementations
impl Surface {
    /// Returns the drawing surface of a given `Window`,
    /// wrapped in a `Result`
    /// 
    /// # Arguments
    ///
    /// * `win` - The `Window` to get the drawing surface from
    ///
    pub fn from_window(win: &mut Window) -> Result<Self, String> {
        unsafe {
            //attempt to get the surface of the window
            let data = sdl::SDL_GetWindowSurface(win.get_data());

            //make sure that the retrieval succeeded
            if data.is_null() { //if the retrieval failed
                //then get the error message
                let err = CStr::from_ptr(sdl::SDL_GetError());

                //and return an error
                return Err(
                    format!(
                        "Could not retrieve window surface! Message: {}",
                           err.to_str().unwrap()));
            }

            //and return a Surface instance
            return Ok(Surface {
                _data: data,
                _is_window_surface: true,
            });
        }
    }

    /// Returns a `Surface` object representing a BMP image,
    /// wrapped in a `Result` 
    ///
    /// # Arguments
    ///
    /// * `path` - The path to the BMP image
    pub fn from_bmp(path: &str) -> Result<Self, String> {
        unsafe {
            //get the image path as a CStr
            let ipath: CString = CString::new(path).unwrap();

            //attempt to get the image file handle
            let option = CString::new("rb").unwrap();
            let rw = sdl::SDL_RWFromFile(ipath.as_ptr(), 
                                            option.as_ptr());

            //validate the file handle
            if rw.is_null() {
                let err = CStr::from_ptr(sdl::SDL_GetError());
                return Err(
                        format!("Unable to load image {}! Message: {}",
                                    path, err.to_str().unwrap()));
            }

            //load the image from the file handle
            let data = sdl::SDL_LoadBMP_RW(rw, 1); 

            //make sure that the image retrieval was successful
            if data.is_null() {
                //get the error message
                let err = CStr::from_ptr(sdl::SDL_GetError());

                //and return an error
                return Err(
                        format!("Unable to load image {}! Message: {}",
                                    path, err.to_str().unwrap()));
            }

            //and return a Surface instance
            return Ok(Surface {
                _data: data,
                _is_window_surface: false,
            });
        }
    }

    /// Returns the `Surface`'s data pointer
    ///
    /// FOR INTERNAL USE ONLY - DO NOT USE
    pub fn get_data(&mut self) -> *mut sdl::SDL_Surface {
        return self._data;
    }

    /// Returns whether the `Surface` was derived from a `Window`
    pub fn is_window_surface(&self) -> bool {
        return self._is_window_surface;
    }

    /// Fills the `Surface` with a given color
    /// 
    /// # Arguments
    ///
    /// * `color` - The color to fill the `Surface` with
    pub fn fill(&mut self, color: &Color) {
        unsafe {
            sdl::SDL_FillRect(self._data, ptr::null(),
                                sdl::SDL_MapRGBA(
                                    (*self._data).format,
                                    color.red,
                                    color.green,
                                    color.blue,
                                    color.alpha));
        }
    }

    /// Blits a graphics element onto the `Surface`
    ///
    /// The `src_rect` and `dst_rect` arguments may be `None`,
    /// in which case the entire area of the corresponding `Surface`
    /// will be used.
    /// 
    /// 
    /// # Arguments
    ///
    /// * `elem` - The graphics element to blit
    /// * `src_rect` - The bounds of the source graphic
    /// * `dst_rect` - The bounds of the target area
    pub fn blit<T: Element>(&mut self, elem: &mut T, 
                                src_rect: Option<Rect>,
                                dst_rect: Option<Rect>) {
        unsafe {
            //get the raw data pointers from the rectangles
            let src_ptr = match src_rect {
                                Some(r) => &r.get_data(),
                                None => ptr::null()
                          };
            let dst_ptr = match dst_rect {
                                Some(r) => &mut r.get_data(),
                                None => ptr::null_mut(),
                          };

            //and finally blit the two surfaces together
            sdl::SDL_UpperBlit(elem.get_surface()._data,
                                    src_ptr, self._data, dst_ptr);
        }
    }
}

//trait implementations
impl Drop for Surface {
    /// Deallocates a `Surface` instance
    fn drop(&mut self) {
        if !self._is_window_surface {
            unsafe {
                sdl::SDL_FreeSurface(self._data);
                self._data = ptr::null_mut();
            }
        }
    }
}

//end of file
