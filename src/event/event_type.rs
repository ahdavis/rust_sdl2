/*
 * event_type.rs
 * Enumerates types of SDL events
 * Created by Andrew Davis
 * Created on 1/27/2019
 * Copyright 2019 Andrew Davis
 * Licensed under the Lesser GNU GPL, version 3
 */

//usage statements
use super::super::c_iface::sdl;
use std::cmp::PartialEq;

/// Defines types of `Event`s triggered by user input
#[derive(PartialEq)]
pub enum EventType {
    /// User-requested quit
    Quit,
    /// Window state change
    WindowEvent,
    /// Key pressed
    KeyDown,
    /// Key released
    KeyUp,
    /// Keyboard text editing detected
    TextEditing,
    /// Keyboard text input detected
    TextInput,
    /// Keyboard character map changed
    KeymapChange,
    /// Mouse moved
    MouseMotion,
    /// Mouse button pressed
    MouseButtonDown,
    /// Mouse button released
    MouseButtonUp,
    /// Mouse wheel moved
    MouseWheel,
    /// Clipboard updated
    ClipboardUpdate,
    /// File dragged and dropped
    DropFile,
    /// Text dragged and dropped
    DropText,
    /// Drag-and-drop operation started
    DropBegin,
    /// Drag-and-drop operation completed
    DropComplete,
    /// Audio device added
    AudioDeviceAdd,
    /// Audio device removed
    AudioDeviceRemove,
    /// Render targets reset
    RenderTargetsReset,
    /// Render device reset
    RenderDeviceReset,
}

//method implementation
impl EventType {
    /// Returns an `EventType` instance from an `SDL_EventType` value
    /// wrapped in a `Result`
    ///
    /// FOR INTERNAL USE ONLY - DO NOT USE
    ///
    /// # Arguments
    ///
    /// * `value` - The `SDL_EventType` value to convert
    pub fn from_sdl(value: &sdl::SDL_EventType) -> Result<Self, String> {
        //match the SDL event value to an EventType instance
        let evnt = match *value {
            sdl::SDL_EventType_SDL_QUIT => {
                Ok(EventType::Quit)
            },
            sdl::SDL_EventType_SDL_WINDOWEVENT => {
                Ok(EventType::WindowEvent)
            }
            sdl::SDL_EventType_SDL_KEYDOWN => {
                Ok(EventType::KeyDown)
            },
            sdl::SDL_EventType_SDL_KEYUP => {
                Ok(EventType::KeyUp)
            },
            sdl::SDL_EventType_SDL_TEXTEDITING => {
                Ok(EventType::TextEditing)
            },
            sdl::SDL_EventType_SDL_TEXTINPUT => {
                Ok(EventType::TextInput)
            },
            sdl::SDL_EventType_SDL_KEYMAPCHANGED => {
                Ok(EventType::KeymapChange)
            },
            sdl::SDL_EventType_SDL_MOUSEMOTION => {
                Ok(EventType::MouseMotion)
            },
            sdl::SDL_EventType_SDL_MOUSEBUTTONDOWN => {
                Ok(EventType::MouseButtonDown)
            },
            sdl::SDL_EventType_SDL_MOUSEBUTTONUP => {
                Ok(EventType::MouseButtonUp)
            },
            sdl::SDL_EventType_SDL_MOUSEWHEEL => {
                Ok(EventType::MouseWheel)
            },
            sdl::SDL_EventType_SDL_CLIPBOARDUPDATE => {
                Ok(EventType::ClipboardUpdate)
            },
            sdl::SDL_EventType_SDL_DROPFILE => {
                Ok(EventType::DropFile)
            },
            sdl::SDL_EventType_SDL_DROPTEXT => {
                Ok(EventType::DropText)
            },
            sdl::SDL_EventType_SDL_DROPBEGIN => {
                Ok(EventType::DropBegin)
            },
            sdl::SDL_EventType_SDL_DROPCOMPLETE => {
                Ok(EventType::DropComplete)
            },
            sdl::SDL_EventType_SDL_AUDIODEVICEADDED => {
                Ok(EventType::AudioDeviceAdd)
            },
            sdl::SDL_EventType_SDL_AUDIODEVICEREMOVED => {
                Ok(EventType::AudioDeviceRemove)
            },
            sdl::SDL_EventType_SDL_RENDER_TARGETS_RESET => {
                Ok(EventType::RenderTargetsReset)
            },
            sdl::SDL_EventType_SDL_RENDER_DEVICE_RESET => {
                Ok(EventType::RenderDeviceReset)
            },
            _ => {
                Err(format!("Unknown event type {}", value))
            }
        };

        //and return it
        return evnt;
    }
}

//trait implementation
impl PartialEq<sdl::SDL_EventType> for EventType {
    /// Compares an `SDL_EventType` value to an `EventType` instance
    /// and returns whether they are equal
    ///
    /// # Arguments
    ///
    /// * `other` - The `SDL_EventType` value to compare against
    #[allow(unused)]
    fn eq(&self, other: &sdl::SDL_EventType) -> bool {
        //convert the argument to an EventType instance
        let other_conv = EventType::from_sdl(other);

        //check it for errors and execute the comparison
        match other_conv {
            Ok(o) => {
                return *self == o;
            }
            Err(s) => {
                return false;
            }
        }
    }

    /// Compares an `SDL_EventType` value to an `EventType` instance
    /// and returns whether they are not equal
    ///
    /// # Arguments
    ///
    /// * `other` - The `SDL_EventType` value to compare against
    fn ne(&self, other: &sdl::SDL_EventType) -> bool {
        return !(self == other);
    }
}

//end of enum

//start of unit tests
#[cfg(test)]
mod tests {
    //usage statements
    use super::EventType;
    use super::super::super::c_iface::sdl;

    //this function tests converting an SDL value to an EventType instance
    #[test]
    fn sdl_to_event() {
        let e = EventType::from_sdl(&sdl::SDL_EventType_SDL_QUIT).unwrap();
        assert!(e == EventType::Quit);
        assert!(e != EventType::KeyDown);
    }

    //this function tests equality between SDL values
    //and EventType instances
    #[test]
    fn sdl_event_eq() {
        let e = EventType::Quit;
        assert!(e == sdl::SDL_EventType_SDL_QUIT);
    }

    //this function tests inequality between SDL values
    //and EventType instances
    #[test]
    fn sdl_event_ne() {
        let e = EventType::Quit;
        assert!(e != sdl::SDL_EventType_SDL_KEYUP);
    }
}

//end of unit tests

//end of file
