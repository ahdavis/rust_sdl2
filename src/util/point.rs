/*
 * point.rs
 * Defines a structure that represents a 2D point
 * Created by Andrew Davis
 * Created on 1/27/2019
 * Copyright 2019 Andrew Davis
 * Licensed under the Lesser GNU GPL, version 3
 */

//usage statements
use super::super::c_iface::sdl;
use std::cmp::PartialEq;
use std::os::raw::c_int;

/// A point in 2D space
#[derive(Clone)]
pub struct Point {
    /// The `Point`'s X-coordinate
    pub x: u32,
    /// The `Point`'s Y-coordinate
    pub y: u32,
}

//method implementations
impl Point {
    /// Returns a new `Point` object
    ///
    /// # Arguments
    ///
    /// * `x` - The X-coordinate of the new `Point`
    /// * `y` - The Y-coordinate of the new `Point`
    pub fn new(x: u32, y: u32) -> Self {
        return Point {
            x: x,
            y: y,
        }
    }

    /// Returns the internal SDL representation of the `Point`
    ///
    /// FOR INTERNAL USE ONLY - DO NOT USE
    pub fn get_data(&self) -> sdl::SDL_Point {
        return sdl::SDL_Point {
            x: self.x as c_int,
            y: self.y as c_int,
        };
    }
}

//Trait implementations
impl PartialEq for Point {
    /// Returns whether two `Point`s are equivalent
    ///
    /// # Arguments
    ///
    /// * `other` - The `Point` to compare `self` to
    fn eq(&self, other: &Point) -> bool {
        return (self.x == other.x) && (self.y == other.y);
    }

    /// Returns whether two `Point`s are not equivalent
    ///
    /// # Arguments
    ///
    /// * `other` - The `Point` to compare `self` to
    fn ne(&self, other: &Point) -> bool {
        return !(self == other);
    }
}

//end of class

//start of unit tests
#[cfg(test)]
mod tests {
    //usage statement
    use super::Point;

    //this test checks equality of two Points
    #[test]
    fn point_eq() {
        let p1 = Point::new(2, 3);
        let p2 = Point::new(2, 3);
        assert!(p1 == p2);
    }

    //this test checks inequality of two Points
    #[test]
    fn point_ne() {
        let p1 = Point::new(2, 3);
        let p2 = Point::new(4, 5);
        assert!(p1 != p2);
    }
}

//end of unit tests

//end of file
