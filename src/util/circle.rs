/*
 * circle.rs
 * Defines a structure that represents a circle
 * Created by Andrew Davis
 * Created on 2/4/2019
 * Copyright 2019 Andrew Davis
 * Licensed under the Lesser GNU GPL, version 3
 */

//usage statements
use super::Point;
use std::f64::consts::PI;
use std::cmp::PartialEq;

/// A geometric circle
pub struct Circle {
    /// The center of the `Circle`
    pub center: Point,
    /// The radius of the `Circle`
    pub radius: f64,
}

//method implementations
impl Circle {
        /// Returns a new `Circle` object with a center point
        /// and a radius, wrapped in a tuple
        ///
        /// # Arguments
        ///
        /// * `args` - A 2- or 3-tuple containing the center point and
        /// radius in that order. The center point can either be
        /// a `Point` reference or discrete `u32` values representing
        /// the x- and y-coordinates of the center point (in that order).
        pub fn new<A: Into<Circle>>(args: A) -> Self {
            return args.into();
        }

        /// Returns the area of a `Circle`
        pub fn area(&self) -> f64 {
            return PI * self.radius.powi(2);
        }

        /// Returns the circumference of a `Circle`
        pub fn circumference(&self) -> f64 {
            return 2.0 * PI * self.radius;
        }

        /// Returns the diameter of a `Circle`
        pub fn diameter(&self) -> f64 {
            return 2.0 * self.radius;
        }

        /// Returns whether a given `Point` is inside a `Circle`
        ///
        /// # Arguments
        ///
        /// * `pt` - The `Point` to check containment against
        pub fn contains_point(&self, pt: &Point) -> bool {
            //get the squared radius
            let rsq = self.radius.powi(2);

            //get the squared distance between the center
            //and the argument point
            let dsq = ((self.center.x as i32 - pt.x as i32).pow(2) +
                    (self.center.y as i32 - pt.y as i32).pow(2)) as f64;

            //and determine whether the point is inside the circle
            return dsq <= rsq;
        }
}

//trait implementations

//PartialEq trait
impl PartialEq for Circle {
    /// Returns whether two `Circle`s are equivalent
    ///
    /// # Arguments
    ///
    /// * `other` - The `Circle` to compare `self` against
    fn eq(&self, other: &Circle) -> bool {
        return (self.center == other.center) &&
                (self.radius == other.radius);
    }

    /// Returns whether two `Circle`s are inequivalent
    ///
    /// # Arguments
    ///
    /// * `other` - The `Circle` to compare `self` against
    fn ne(&self, other: &Circle) -> bool {
        return !(self == other);
    }
}

//From<(Point, f64)> trait 
impl From<(Point, f64)> for Circle {
    /// Constructs a `Circle` object from a center `Point` and a radius
    ///
    /// # Arguments
    ///
    /// * `center` - The center `Point` of the `Circle` 
    /// * `radius` - The radius of the `Circle`
    fn from((center, radius): (Point, f64)) -> Self {
        return Circle {
            center: center.clone(),
            radius: radius
        };
    }
}

//From<(u32, u32, f64)> trait
impl From<(u32, u32, f64)> for Circle {
    /// Constructs a `Circle` object from two coordinates and a radius
    /// 
    /// # Arguments
    ///
    /// * `x` - The x-coord of the `Circle`'s center
    /// * `y` - The y-coord of the `Circle`'s center
    /// * `radius` - The radius of the `Circle`
    fn from((x, y, radius): (u32, u32, f64)) -> Self {
        return Circle {
            center: Point::new(x, y),
            radius: radius 
        };
    }
}

//Default trait
impl Default for Circle {
    /// Returns the unit circle (radius 1.0, centered at (0,0))
    fn default() -> Self {
        return Circle::new((0, 0, 1.0));
    }
}

//end of class

//start of unit tests
#[cfg(test)]
mod tests {
    //usage statement
    use super::Circle;
    use super::super::Point;

    //this function makes sure that the default Circle is a unit circle
    #[test]
    fn default_is_unit() {
        let c: Circle = Default::default();
        assert_eq!(c.center.x, 0);
        assert_eq!(c.center.y, 0);
        assert_eq!(c.radius, 1.0);
    }

    //this function tests creating a Circle two different ways
    #[test]
    fn variant_creation() {
        let c1 = Circle::new((Point::new(1, 4), 5.0));
        let c2 = Circle::new((1, 4, 5.0));
        assert!(c1 == c2);
    }

    //this function tests Circle::contains_point()
    #[test]
    fn contains_point() {
        let pt = Point::new(3, 3);
        let c = Circle::new((2, 2, 5.0));
        assert!(c.contains_point(&pt));
    }
}

//end of unit tests

//end of file
