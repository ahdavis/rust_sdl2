/*
 * color.rs
 * Defines a class that represents a color
 * Created by Andrew Davis
 * Created on 1/20/2019
 * Copyright 2019 Andrew Davis
 * Licensed under the Lesser GNU GPL, version 3
 */

//usage statement
use super::super::c_iface::sdl;

/// A color defined by red, green, blue, and alpha components
pub struct Color {
    /// How much red is in the `Color` 
    pub red: u8,
    /// How much green is in the `Color`
    pub green: u8,
    /// How much blue is in the `Color`
    pub blue: u8,
    /// How opaque the `Color` is
    pub alpha: u8,
}

//method implementations
impl Color {
    /// Returns a `Color` object defined by its RGB components
    /// 
    /// # Arguments
    ///
    /// * `r` - The red value of the `Color`
    /// * `g` - The green value of the `Color`
    /// * `b` - The blue value of the `Color` 
    pub fn from_rgb(r: u8, g: u8, b: u8) -> Self {
        //call the from_rgba method with an alpha value of 255
        return Color::from_rgba(r, g, b, 0xFF);
    }

    /// Returns a `Color` object defined by its RGBA components
    ///
    /// # Arguments
    ///
    /// * `r` - The red value of the `Color`
    /// * `g` - The green value of the `Color`
    /// * `b` - The blue value of the `Color`
    /// * `a` - The alpha value (opacity) of the `Color` 
    pub fn from_rgba(r: u8, g: u8, b: u8, a: u8) -> Self {
        //return a Color object
        return Color {
            red: r,
            green: g,
            blue: b,
            alpha: a,
        };
    }

    /// Converts a `Color` object to an `SDL_Color` instance
    ///
    /// FOR INTERNAL USE ONLY - DO NOT USE
    pub fn get_data(&self) -> sdl::SDL_Color {
        return sdl::SDL_Color {
            r: self.red,
            g: self.green,
            b: self.blue,
            a: self.alpha,
        };
    }
}

//trait implementations
impl Default for Color {
    /// Returns a `Color` initialized to opaque white
    fn default() -> Self {
        return Color::from_rgba(0xFF, 0xFF, 0xFF, 0xFF);
    }
}

//end of class

//start of unit tests
#[cfg(test)]
mod tests {
    use super::Color;

    //this function tests the default color method
    //to make sure that it is opaque white
    #[test]
    fn default_is_white() {
        let color: Color = Default::default();
        assert_eq!(color.red, 0xFF);
        assert_eq!(color.green, 0xFF);
        assert_eq!(color.blue, 0xFF);
        assert_eq!(color.alpha, 0xFF);
    }

    //this function tests to make sure that
    //colors created without specifying an
    //alpha value are always fully opaque
    #[test]
    fn color_is_opaque() {
        let color = Color::from_rgb(0x00, 0x00, 0x00);
        assert_eq!(color.alpha, 0xFF);
    }
}

//end of file
