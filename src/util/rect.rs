/*
 * rect.rs
 * Defines a structure that represents a rectangle
 * Created by Andrew Davis
 * Created on 1/24/2019
 * Copyright 2019 Andrew Davis
 * Licensed under the Lesser GNU GPL, version 3
 */

//usage statements
use super::super::c_iface::sdl;
use std::cmp::PartialEq;
use std::os::raw::c_int;
use super::Point;

/// A rectangle defined by width, height, and location
pub struct Rect {
    /// The x-coordinate of the `Rect`
    pub x: u32,
    /// The y-coordinate of the `Rect` 
    pub y: u32,
    /// The width of the `Rect`
    pub width: u32,
    /// The height of the `Rect` 
    pub height: u32,
}

//method implementations
impl Rect {
    /// Returns a new `Rect` object
    ///
    /// # Arguments
    ///
    /// * `x` - The x-coordinate of the `Rect`
    /// * `y` - The y-coordinate of the `Rect`
    /// * `width` - The width of the `Rect`
    /// * `height` - The height of the `Rect` 
    pub fn new(x: u32, y: u32, width: u32, height: u32) -> Self {
        return Rect {
            x: x,
            y: y,
            width: width,
            height: height,
        };
    }

    /// Returns the SDL data for a `Rect`
    ///
    /// FOR INTERNAL USE ONLY - DO NOT USE
    pub fn get_data(&self) -> sdl::SDL_Rect {
        return sdl::SDL_Rect {
            x: self.x as c_int,
            y: self.y as c_int,
            w: self.width as c_int,
            h: self.height as c_int,
        };
    }

    /// Returns whether one `Rect` collides with another
    ///
    /// # Arguments
    ///
    /// * `other` - The `Rect` to check collision against
    pub fn collides_with(&self, other: &Rect) -> bool {
        unsafe {
            let self_data = self.get_data();
            let other_data = other.get_data();
            let mut result = sdl::SDL_Rect {x: 0, y: 0, w: 1, h: 1};

            let collides = sdl::SDL_IntersectRect(&self_data,
                                                        &other_data,
                                                        &mut result);

            match collides {
                sdl::SDL_bool_SDL_TRUE => return true,
                sdl::SDL_bool_SDL_FALSE => return false, 
                _ => return false
            }
        }
    }

    /// Returns whether a `Point` object is inside the `Rect`
    ///
    /// # Arguments
    ///
    /// * `point` - The `Point` to check containment for
    pub fn contains_point(&self, point: &Point) -> bool {
        let within_x = (point.x >= self.x) && (point.x <= self.width);
        let within_y = (point.y >= self.y) && (point.y <= self.height);
        return within_x && within_y;
    }
}

//trait implementation

//allows for comparison of two Rects
impl PartialEq for Rect {
    /// Returns whether two `Rect`s are equivalent
    ///
    /// # Arguments
    ///
    /// * `other` - The `Rect` to compare `self` to
    fn eq(&self, other: &Rect) -> bool {
        let x_eq = self.x == other.x;
        let y_eq = self.y == other.y;
        let w_eq = self.width == other.width;
        let h_eq = self.height == other.height;
        return (x_eq && y_eq) && (w_eq && h_eq);
    }

    /// Returns whether two `Rect`s are not equivalent
    ///
    /// # Arguments
    ///
    /// * `other` - The `Rect` to compare `self` to
    fn ne(&self, other: &Rect) -> bool {
        return !(self == other);
    }
}

//end of class

//start of unit tests
#[cfg(test)]
mod tests {
    use super::Rect;
    use super::super::Point;

    //This test checks equality of two Rects
    #[test]
    fn is_equal() {
        let r1 = Rect::new(0, 0, 2, 3);
        let r2 = Rect::new(0, 0, 2, 3);
        assert!(r1 == r2);
    }

    //This test checks inequality of two Rects
    #[test]
    fn is_not_equal() {
        let r1 = Rect::new(0, 0, 2, 3);
        let r2 = Rect::new(0, 0, 3, 2);
        assert!(r1 != r2);
    }

    //This test checks collision of two Rects
    #[test]
    fn collides_with() {
        let r1 = Rect::new(0, 0, 10, 10);
        let r2 = Rect::new(5, 5, 20, 20);
        assert!(r1.collides_with(&r2));
    }

    //This test checks point containment
    #[test]
    fn contains_point() {
        let r = Rect::new(0, 0, 20, 20);
        let p = Point::new(5, 10);
        assert!(r.contains_point(&p));
    }
}

//end of unit tests
