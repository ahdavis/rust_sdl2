/*
 * mod.rs
 * Module header for the SDL2 C interface
 * Created by Andrew Davis
 * Created on 1/20/2019
 * Copyright 2019 Andrew Davis
 * Licensed under the Lesser GNU GPL, version 3
 */

//suppress warnings
#![allow(non_upper_case_globals)]
#![allow(non_camel_case_types)]
#![allow(non_snake_case)]
#![allow(unused)]

//module imports
pub mod sdl;
pub mod img;
pub mod ttf;
pub mod mix;

//end of header
