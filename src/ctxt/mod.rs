/*
 * mod.rs
 * Module header for rust_sdl2 context files
 * Created by Andrew Davis
 * Created on 1/20/2019
 * Copyright 2019 Andrew Davis
 * Licensed under the Lesser GNU GPL, version 3
 */

//module import statement
mod context;

//usage statement
pub use context::Context;

//end of header
