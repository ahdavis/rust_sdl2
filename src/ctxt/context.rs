/*
 * context.rs
 * Defines a class that holds contextual information for rust_sdl2
 * Created by Andrew Davis
 * Created on 1/20/2019
 * Copyright 2019 Andrew Davis
 * Licensed under the Lesser GNU GPL, version 3
 */

//usage statements
use super::super::c_iface::*;
use std::ffi::CStr;

/// Used to access contextual data
pub struct Context {
    _flags: i32,
}

// method implementations
impl Context {
    /// Returns a new `Context` object
    /// 
    /// # Panics
    ///
    /// This method will panic if the SDL system cannot be initialized
    pub fn new() -> Self {  
        //store the init flags in a variable
        let init_flags = sdl::SDL_INIT_EVERYTHING;

        //attempt to initialize SDL
        let res = unsafe {
            sdl::SDL_Init(init_flags)
        };

        //make sure that initialization succeeded
        if res < 0 { //if initialization failed
           //then convert the error message to an object
           let err = unsafe {
               CStr::from_ptr(sdl::SDL_GetError())
           };

           //and panic with the message
           panic!("SDL failed to initialize! Message: {}", 
                    err.to_str().unwrap());
        }

        //and return the new Context object
        return Context {
            _flags: init_flags as i32,
        };

    }

    /// Delays the calling thread for a given number of milliseconds
    ///
    /// # Arguments
    ///
    /// * `ms` - The number of milliseconds to delay for
    pub fn delay(&self, ms: u32) {
        unsafe {
            sdl::SDL_Delay(ms);
        }
    }
}

// trait implementations

// Drop trait implementation
impl Drop for Context {
    /// Shuts down SDL when the `Context` goes out of scope
    fn drop(&mut self) {
        unsafe {
            sdl::SDL_Quit();
        }
    }
}

//end of file
