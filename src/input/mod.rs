/*
 * mod.rs
 * Module header for input files
 * Created by Andrew Davis
 * Created on 1/28/2019
 * Copyright 2019 Andrew Davis
 * Licensed under the Lesser GNU GPL, version 3
 */

//module import statements
mod keycode;

//usage statements
pub use keycode::Keycode;

//end of file
