/*
 * keycode.rs
 * Enumerates input keycodes
 * Created by Andrew Davis
 * Created on 1/28/2019
 * Copyright 2019 Andrew Davis
 * Licensed under the Lesser GNU GPL, version 3
 */

//usage statements
use super::super::c_iface::sdl;
use std::cmp::PartialEq;

/// Used to denote keys on the keyboard
#[derive(PartialEq)]
pub enum Keycode {
    /// Numeric 0
    Zero,
    /// Numeric 1
    One,
    /// Numeric 2
    Two,
    /// Numeric 3
    Three,
    /// Numeric 4
    Four,
    /// Numeric 5
    Five,
    /// Numeric 6
    Six,
    /// Numeric 7
    Seven,
    /// Numeric 8
    Eight,
    /// Numeric 9
    Nine,
    /// Letter A
    A,
    /// Apostrophe
    Apostrophe,
    /// Context (Windows) key
    Context,
    /// Letter B
    B,
    /// Backslash
    Backslash,
    /// Backspace
    Backspace,
    /// Letter C
    C,
    /// Caps Lock
    CapsLock,
    /// Comma
    Comma,
    /// Letter D
    D,
    /// Decimal separator key
    DecimalSeparator,
    /// Delete
    Delete,
    /// Down arrow key 
    Down,
    /// Letter E
    E,
    /// Equals key
    Equals,
    /// Escape
    Escape,
    /// Letter F
    F,
    /// Function key 1
    Fn1,
    /// Function key 2
    Fn2,
    /// Function key 3
    Fn3,
    /// Function key 4
    Fn4,
    /// Function key 5
    Fn5,
    /// Function key 6
    Fn6,
    /// Function key 7
    Fn7,
    /// Function key 8
    Fn8,
    /// Function key 9
    Fn9,
    /// Function key 10
    Fn10,
    /// Function key 11
    Fn11,
    /// Function key 12
    Fn12,
    /// Letter G
    G,
    /// Backquote
    Backquote,
    /// Letter H
    H,
    /// Letter I
    I,
    /// Insert
    Insert,
    /// Letter J
    J,
    /// Letter K,
    K,
    /// Letter L
    L,
    /// Left Alt
    LAlt,
    /// Left Ctrl
    LCtrl,
    /// Left arrow key
    Left,
    /// Left bracket
    LBracket,
    /// Left GUI key
    LGUI,
    /// Left Shift key
    LShift,
    /// Letter M
    M,
    /// Minus key
    Minus,
    /// Letter N
    N,
    /// Num Lock
    NumLock,
    /// Letter O
    O,
    /// Letter P
    P,
    /// Page Down key
    PgDown,
    /// Page Up key
    PgUp,
    /// Pause key
    Pause,
    /// Period
    Period,
    /// Letter Q
    Q,
    /// Letter R
    R,
    /// Right Alt 
    RAlt,
    /// Right Ctrl
    RCtrl,
    /// Return (Enter)
    Return,
    /// Right GUI key
    RGUI,
    /// Right arrow key 
    Right,
    /// Right bracket
    RBracket,
    /// Right Shift key
    RShift,
    /// Letter S
    S,
    /// Semicolon key
    Semicolon,
    /// Forward slash
    ForwardSlash,
    /// Spacebar
    Spacebar,
    /// Letter T
    T,
    /// Tab
    Tab,
    /// Letter U
    U,
    /// Up arrow key
    Up,
    /// Letter V
    V,
    /// Letter W
    W,
    /// Letter X
    X,
    /// Letter Y
    Y,
    /// Letter Z
    Z,
    /// Ampersand
    Ampersand,
    /// Asterisk
    Asterisk,
    /// @ sign
    At,
    /// Caret (^)
    Caret,
    /// Colon 
    Colon,
    /// Dollar sign
    Dollar,
    /// Exclamation mark
    ExclaimMark,
    /// Greater-than sign
    GreaterThan,
    /// Hash mark
    Hash,
    /// Left parenthesis
    LParen,
    /// Less-than sign
    LessThan,
    /// Percent sign
    Percent,
    /// Plus sign
    Plus,
    /// Question mark 
    Question,
    /// Double quote
    DblQuote,
    /// Right parenthesis
    RParen,
    /// Underscore
    Underscore
}

//method implementations
impl Keycode {
    /// Returns a `Keycode` instance (wrapped in a `Result`)
    /// derived from its corresponding SDL keycode
    /// 
    /// # Arguments
    ///
    /// * `key` - The SDL keycode to convert
    pub fn from_sdl(key: &sdl::SDL_Keycode) -> Result<Self, String> {
        //match the input key
        let key_conv = match *key {
            sdl::SDLK_0 => {
                Ok(Keycode::Zero)
            },
            sdl::SDLK_1 => {
                Ok(Keycode::One)
            },
            sdl::SDLK_2 => {
                Ok(Keycode::Two)
            }
            sdl::SDLK_3 => {
                Ok(Keycode::Three)
            },
            sdl::SDLK_4 => {
                Ok(Keycode::Four)
            },
            sdl::SDLK_5 => {
                Ok(Keycode::Five)
            },
            sdl::SDLK_6 => {
                Ok(Keycode::Six)
            },
            sdl::SDLK_7 => {
                Ok(Keycode::Seven)
            },
            sdl::SDLK_8 => {
                Ok(Keycode::Eight)
            },
            sdl::SDLK_9 => {
                Ok(Keycode::Nine)
            },
            sdl::SDLK_a => {
                Ok(Keycode::A)
            },
            sdl::SDLK_QUOTE => {
                Ok(Keycode::Apostrophe)
            },
            sdl::SDLK_b => {
                Ok(Keycode::B)
            },
            sdl::SDLK_BACKSLASH => {
                Ok(Keycode::Backslash)
            },
            sdl::SDLK_BACKSPACE => {
                Ok(Keycode::Backspace)
            },
            sdl::SDLK_c => {
                Ok(Keycode::C)
            },
            sdl::SDLK_CAPSLOCK => {
                Ok(Keycode::CapsLock)
            },
            sdl::SDLK_COMMA => {
                Ok(Keycode::Comma)
            },
            sdl::SDLK_d => {
                Ok(Keycode::D)
            },
            sdl::SDLK_DECIMALSEPARATOR => {
                Ok(Keycode::DecimalSeparator)
            },
            sdl::SDLK_DELETE => {
                Ok(Keycode::Delete)
            },
            sdl::SDLK_DOWN => {
                Ok(Keycode::Down)
            },
            sdl::SDLK_e => {
                Ok(Keycode::E)
            },
            sdl::SDLK_EQUALS => {
                Ok(Keycode::Equals)
            },
            sdl::SDLK_ESCAPE => {
                Ok(Keycode::Escape)
            },
            sdl::SDLK_f => {
                Ok(Keycode::F)
            },
            sdl::SDLK_F1 => {
                Ok(Keycode::Fn1)
            },
            sdl::SDLK_F2 => {
                Ok(Keycode::Fn2)
            },
            sdl::SDLK_F3 => {
                Ok(Keycode::Fn3)
            },
            sdl::SDLK_F4 => {
                Ok(Keycode::Fn4)
            },
            sdl::SDLK_F5 => {
                Ok(Keycode::Fn5)
            },
            sdl::SDLK_F6 => {
                Ok(Keycode::Fn6)
            },
            sdl::SDLK_F7 => {
                Ok(Keycode::Fn7)
            },
            sdl::SDLK_F8 => {
                Ok(Keycode::Fn8)
            },
            sdl::SDLK_F9 => {
                Ok(Keycode::Fn9)
            },
            sdl::SDLK_F10 => {
                Ok(Keycode::Fn10)
            },
            sdl::SDLK_F11 => {
                Ok(Keycode::Fn11)
            },
            sdl::SDLK_F12 => {
                Ok(Keycode::Fn12)
            },
            sdl::SDLK_g => {
                Ok(Keycode::G)
            },
            sdl::SDLK_BACKQUOTE => {
                Ok(Keycode::Backquote)
            },
            sdl::SDLK_h => {
                Ok(Keycode::H)
            },
            sdl::SDLK_i => {
                Ok(Keycode::I)
            },
            sdl::SDLK_INSERT => {
                Ok(Keycode::Insert)
            },
            sdl::SDLK_j => {
                Ok(Keycode::J)
            },
            sdl::SDLK_k => {
                Ok(Keycode::K)
            },
            sdl::SDLK_l => {
                Ok(Keycode::L)
            },
            sdl::SDLK_LALT => {
                Ok(Keycode::LAlt)
            },
            sdl::SDLK_LCTRL => {
                Ok(Keycode::LCtrl)
            },
            sdl::SDLK_LEFT => {
                Ok(Keycode::Left)
            },
            sdl::SDLK_LEFTBRACKET => {
                Ok(Keycode::LBracket)
            },
            sdl::SDLK_LGUI => {
                Ok(Keycode::LGUI)
            },
            sdl::SDLK_LSHIFT => {
                Ok(Keycode::LShift)
            },
            sdl::SDLK_m => {
                Ok(Keycode::M)
            },
            sdl::SDLK_MINUS => {
                Ok(Keycode::Minus)
            },
            sdl::SDLK_n => {
                Ok(Keycode::N)
            },
            sdl::SDLK_NUMLOCKCLEAR => {
                Ok(Keycode::NumLock)
            },
            sdl::SDLK_o => {
                Ok(Keycode::O)
            },
            sdl::SDLK_p => {
                Ok(Keycode::P)
            },
            sdl::SDLK_PAGEUP => {
                Ok(Keycode::PgUp)
            },
            sdl::SDLK_PAGEDOWN => {
                Ok(Keycode::PgDown)
            },  
            sdl::SDLK_PAUSE => {
                Ok(Keycode::Pause)
            },
            sdl::SDLK_PERIOD => {
                Ok(Keycode::Period)
            },
            sdl::SDLK_q => {
                Ok(Keycode::Q)
            },
            sdl::SDLK_r => {
                Ok(Keycode::R)
            },
            sdl::SDLK_RALT => {
                Ok(Keycode::RAlt)
            },
            sdl::SDLK_RCTRL => {
                Ok(Keycode::RCtrl)
            },
            sdl::SDLK_RETURN => {
                Ok(Keycode::Return)
            },
            sdl::SDLK_RETURN2 => {
                Ok(Keycode::Return)
            },
            sdl::SDLK_RGUI => {
                Ok(Keycode::RGUI)
            },
            sdl::SDLK_RIGHT => {
                Ok(Keycode::Right)
            },
            sdl::SDLK_RIGHTBRACKET => {
                Ok(Keycode::RBracket)
            },
            sdl::SDLK_RSHIFT => {
                Ok(Keycode::RShift)
            },
            sdl::SDLK_s => {
                Ok(Keycode::S)
            },
            sdl::SDLK_SEMICOLON => {
                Ok(Keycode::Semicolon)
            },
            sdl::SDLK_SLASH => {
                Ok(Keycode::ForwardSlash)
            },
            sdl::SDLK_SPACE => {
                Ok(Keycode::Spacebar)
            },
            sdl::SDLK_t => {
                Ok(Keycode::T)
            },
            sdl::SDLK_TAB => {
                Ok(Keycode::Tab)
            },
            sdl::SDLK_u => {
                Ok(Keycode::U)
            },
            sdl::SDLK_UP => {
                Ok(Keycode::Up)
            },
            sdl::SDLK_v => {
                Ok(Keycode::V)
            },
            sdl::SDLK_w => {
                Ok(Keycode::W)
            },
            sdl::SDLK_x => {
                Ok(Keycode::X)
            },
            sdl::SDLK_y => {
                Ok(Keycode::Y)
            },
            sdl::SDLK_z => {
                Ok(Keycode::Z)
            },
            sdl::SDLK_AMPERSAND => {
                Ok(Keycode::Ampersand)
            },
            sdl::SDLK_ASTERISK => {
                Ok(Keycode::Asterisk)
            },
            sdl::SDLK_AT => {
                Ok(Keycode::At)
            },
            sdl::SDLK_CARET => {
                Ok(Keycode::Caret)
            },
            sdl::SDLK_COLON => {
                Ok(Keycode::Colon)
            },
            sdl::SDLK_DOLLAR => {
                Ok(Keycode::Dollar)
            },
            sdl::SDLK_EXCLAIM => {
                Ok(Keycode::ExclaimMark)
            },
            sdl::SDLK_GREATER => {
                Ok(Keycode::GreaterThan)
            },
            sdl::SDLK_HASH => {
                Ok(Keycode::Hash)
            },
            sdl::SDLK_LEFTPAREN => {
                Ok(Keycode::LParen)
            },
            sdl::SDLK_LESS => {
                Ok(Keycode::LessThan)
            },
            sdl::SDLK_PERCENT => {
                Ok(Keycode::Percent)
            },
            sdl::SDLK_PLUS => {
                Ok(Keycode::Plus)
            },
            sdl::SDLK_QUESTION => {
                Ok(Keycode::Question)
            },
            sdl::SDLK_QUOTEDBL => {
                Ok(Keycode::DblQuote)
            },
            sdl::SDLK_RIGHTPAREN => {
                Ok(Keycode::RParen)
            },
            sdl::SDLK_UNDERSCORE => {
                Ok(Keycode::Underscore)
            },
            _ => {
                Err(format!("Invalid keycode {}", key))
            }
        };

        //and return it
        return key_conv;
    }
}

//trait implementations
impl PartialEq<sdl::SDL_Keycode> for Keycode {
    /// Returns whether a `Keycode` instance
    /// is equivalent to an `SDL_Keycode` value
    ///
    /// # Arguments
    ///
    /// * `value` - The `SDL_Keycode` value to check against
    #[allow(unused)]
    fn eq(&self, value: &sdl::SDL_Keycode) -> bool {
        //convert the value to a Keycode instance
        let value_conv = Keycode::from_sdl(value);

        //check it for errors and compare it
        match value_conv {
            Ok(v) => {
                return *self == v;
            },
            Err(s) => {
                return false;
            }
        }
    }

    /// Returns whether a `Keycode` instance
    /// is equivalent to an `SDL_Keycode` value
    /// 
    /// # Arguments
    ///
    /// * `value` - The `SDL_Keycode` value to check against
    fn ne(&self, value: &sdl::SDL_Keycode) -> bool {
        return !(self == value);
    }
}

//end of enum

//start of unit tests

#[cfg(test)]
mod tests {
    //usage statements
    use super::Keycode;
    use super::super::super::c_iface::sdl;

    //this function tests keycode equivalence
    #[test]
    fn keycode_eq() {
        assert!(Keycode::Down == Keycode::Down);
    }

    //this function tests keycode inequivalence
    #[test]
    fn keycode_ne() {
        assert!(Keycode::Down != Keycode::Up);
    }

    //this function tests keycode to SDL equivalence
    #[test]
    fn keycode_sdl_eq() {
        let key = Keycode::from_sdl(&sdl::SDLK_DOWN).unwrap();
        assert!(Keycode::Down == key);
    }

    //this function tests keycode to SDL inequivalence
    #[test]
    fn keycode_sdl_ne() {
        let key = Keycode::from_sdl(&sdl::SDLK_UP).unwrap();
        assert!(Keycode::Down != key);
    }
}

//end of unit tests

//end of file
