/*
 * quit.rs
 * Defines a test for quit events
 * Created by Andrew Davis
 * Created on 1/28/2019
 * Copyright 2019 Andrew Davis
 * Licensed under the Lesser GNU GPL, version 3
 */

//crate import statement
extern crate rust_sdl2;

//usage statements
use rust_sdl2::Context;
use rust_sdl2::ImgElement;
use rust_sdl2::Surface;
use rust_sdl2::Window;
use rust_sdl2::Event;
use rust_sdl2::EventType;

//test function
#[test]
fn quit() {
    //initialize variables
    let _ctxt_ = Context::new();
    let mut win = Window::new("Event Test", 640, 480, true).unwrap();
    let mut win_surf = Surface::from_window(&mut win).unwrap();
    let mut img = ImgElement::from_bmp("./assets/x.bmp").unwrap();
    let mut quit = false;
    
    //main loop
    while !quit {
        //event loop
        loop {
            let event = Event::poll();
            match event {
                Some(e) => {
                    if e.triggered_by(EventType::Quit) {
                        quit = true;
                    }
                }

                None => {
                    break;
                }
            }
        }

        //display the image
        win_surf.blit(&mut img, None, None);
        win.update();
    }
}

//end of test
