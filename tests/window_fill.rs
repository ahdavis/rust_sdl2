/*
 * window_fill.rs
 * Defines a test for filling an SDL window
 * Created by Andrew Davis
 * Created on 1/20/2019
 * Copyright 2019 Andrew Davis
 * Licensed under the Lesser GNU GPL, version 3
 */

//crate import statement
extern crate rust_sdl2;

//usage statement
use rust_sdl2::Context;
use rust_sdl2::Window;
use rust_sdl2::Surface;
use rust_sdl2::Color;

//test function
#[test]
fn window_fill() {
    let ctxt = Context::new();
    let mut win = Window::new("Window Fill Test", 640, 480, true).unwrap();
    let mut surf = Surface::from_window(&mut win).unwrap();
    let color = Color::from_rgb(0xFF, 0x00, 0x00); //red
    surf.fill(&color);
    win.update();
    ctxt.delay(2000);
}

//end of file
