/*
 * window_caption.rs
 * Tests caption modification of windows
 * Created by Andrew Davis
 * Created on 1/22/2019
 * Copyright 2019 Andrew Davis
 * Licensed under the Lesser GNU GPL, version 3
 */

//crate import statement
extern crate rust_sdl2;

//usage statements
use rust_sdl2::Context;
use rust_sdl2::Window;

//test function
#[test]
fn window_caption() {
    let ctxt = Context::new();
    let mut window = Window::new("First Caption", 640, 480, true).unwrap();
    assert_eq!(window.get_caption(), "First Caption");
    ctxt.delay(2000);
    window.set_caption("Second Caption");
    assert_eq!(window.get_caption(), "Second Caption");
    ctxt.delay(2000);
}

//end of file
