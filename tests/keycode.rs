/*
 * keycode.rs
 * Defines a test for key events
 * Created by Andrew Davis
 * Created on 1/25/2019
 * Copyright 2019 Andrew Davis
 * Licensed under the Lesser GNU GPL, version 3
 */

//crate import statement
extern crate rust_sdl2;

//usage statements
use rust_sdl2::Context;
use rust_sdl2::Event;
use rust_sdl2::EventType;
use rust_sdl2::ImgElement;
use rust_sdl2::Window;
use rust_sdl2::Surface;
use rust_sdl2::Keycode;

//test function
#[test]
fn keycode() {
    //define constants to use as vector indices
    const DEFAULT: usize = 0;
    const UP: usize = 1;
    const DOWN: usize = 2;
    const LEFT: usize = 3;
    const RIGHT: usize = 4;

    //get the SDL context
    let _ctxt_ = Context::new();

    //create the window
    let mut win = Window::new("Keycode Test", 640, 480, true).unwrap();

    //get the window surface
    let mut win_surf = Surface::from_window(&mut win).unwrap();

    //load the image surfaces
    let press = ImgElement::from_bmp("./assets/press.bmp").unwrap();
    let down = ImgElement::from_bmp("./assets/down.bmp").unwrap();
    let up = ImgElement::from_bmp("./assets/up.bmp").unwrap();
    let left = ImgElement::from_bmp("./assets/left.bmp").unwrap();
    let right = ImgElement::from_bmp("./assets/right.bmp").unwrap();

    //vectorize them
    let mut images = vec![press, up, down, left, right];

    //get the current image
    let mut cur_img = &mut images[DEFAULT];

    //declare a sentinel variable
    let mut quit = false;

    //main loop
    while !quit {
        //event loop
        loop {
            //get the current event
            let evnt = Event::poll();

            //process the event
            match evnt {
                Some(e) => {
                    if e.triggered_by(EventType::Quit) {
                        quit = true;
                    } else if e.triggered_by(EventType::KeyDown) {
                        match e.get_keycode() {
                            Ok(k) => {
                                match k {
                                    Keycode::Up => {
                                        cur_img = &mut images[UP];
                                    }
                                    Keycode::Down => {
                                        cur_img = &mut images[DOWN];
                                    }
                                    Keycode::Left => {
                                        cur_img = &mut images[LEFT];
                                    }
                                    Keycode::Right => {
                                        cur_img = &mut images[RIGHT];
                                    }
                                    _ => {
                                        cur_img = &mut images[DEFAULT];
                                    }
                                }
                            }
                            Err(s) => {
                                println!("{}", s);
                            }
                        }
                    }
                }

                None => {
                    break;
                }
            }

        }

        //draw the image
        win_surf.blit(&mut *cur_img, None, None);

        //and update the window
        win.update();
    }
}

//end of test
