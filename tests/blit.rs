/*
 * blit.rs
 * Defines a test for image blitting
 * Created by Andrew Davis
 * Created on 1/25/2019
 * Copyright 2019 Andrew Davis
 * Licensed under the Lesser GNU GPL, version 3
 */

//crate import statement
extern crate rust_sdl2;

//usage statements
use rust_sdl2::Context;
use rust_sdl2::ImgElement;
use rust_sdl2::Surface;
use rust_sdl2::Window;

//test function
#[test]
fn blit() {
    let ctxt = Context::new();
    let mut win = Window::new("Blit Test", 640, 480, true).unwrap();
    let mut win_surf = Surface::from_window(&mut win).unwrap();
    let mut img = ImgElement::from_bmp("./assets/hello_world.bmp")
        .unwrap();
    win_surf.blit(&mut img, None, None);
    win.update();
    ctxt.delay(2000);
}

//end of test
