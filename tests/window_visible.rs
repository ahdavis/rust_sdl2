/*
 * window_visible.rs
 * Tests visibility modification of windows
 * Created by Andrew Davis
 * Created on 1/22/2019
 * Copyright 2019 Andrew Davis
 * Licensed under the Lesser GNU GPL, version 3
 */

//crate import statement
extern crate rust_sdl2;

//usage statements
use rust_sdl2::Window;
use rust_sdl2::Context;

//test function
#[test]
fn window_visible() {
    let ctxt = Context::new();
    let mut win = Window::new("Window Visibility Test", 640, 480, false)
                                .unwrap();
    assert_eq!(win.is_shown(), false);
    win.show();
    win.update();
    assert_eq!(win.is_shown(), true);
    ctxt.delay(1000);
    win.hide();
    win.update();
    assert_eq!(win.is_shown(), false);
}

//end of test
