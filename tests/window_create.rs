/*
 * window_create.rs
 * Defines a test for creating an SDL window
 * Created by Andrew Davis
 * Created on 1/20/2019
 * Copyright 2019 Andrew Davis
 * Licensed under the Lesser GNU GPL, version 3
 */

//crate import statement
extern crate rust_sdl2;

//usage statement
use rust_sdl2::Context;
use rust_sdl2::Window;

//test function
#[test]
fn window_create() {
    let ctxt = Context::new();
    let mut win = Window::new("Window Creation Test", 640, 480, true)
                                .unwrap();
    win.update();
    ctxt.delay(2000);
}

//end of file
