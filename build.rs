/*
 * build.rs
 * Build script for rust_sdl2
 * Created by Andrew Davis
 * Created on 1/20/2019
 * Copyright 2019 Andrew Davis
 * Licensed under the Lesser GNU GPL, version 3
 */

//no imports

//main function - main entry point for the script
fn main() {
    //emit build options
    println!("cargo:rustc-link-lib=SDL2");
    println!("cargo:rustc-link-lib=SDL2_image");
    println!("cargo:rustc-link-lib=SDL2_ttf");
    println!("cargo:rustc-link-lib=SDL2_mixer");
}

//end of script
